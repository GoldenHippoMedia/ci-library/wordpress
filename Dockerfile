FROM registry.gitlab.com/goldenhippomedia/docker-library/wordpress/v5-8-2

# disable ipv6
RUN echo 'precedence ::ffff:0:0/96 100' >> /etc/gai.conf

# install WordPress plugins and themes
COPY . /usr/src/wordpress/
RUN set -ex; \
    cd /usr/src/wordpress; \
    composer self-update 2.6.6; \
    composer update --no-interaction --no-progress; \
    rm -rf composer.json auth.json vendor/
